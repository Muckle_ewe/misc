package misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

abstract class Parser implements Runnable {
	protected ConcurrentHashMap<String, ArrayList<String>> outputMap;
	protected final LinkedBlockingQueue< Pair<String, BufferedReader> > htmlPages;
	protected volatile boolean keepReading = true;
	
	public Parser(LinkedBlockingQueue< Pair<String, BufferedReader> > q) {
		htmlPages = q;
	}
	
	public void passOutputMap(ConcurrentHashMap<String, ArrayList<String>> conHashMap) {
		outputMap = conHashMap;
	}
	
	void stopReading() {
		keepReading = false;
	}
	
	public void run() {
		while (keepReading || !htmlPages.isEmpty()) {
			if (!htmlPages.isEmpty()) {
				Pair<String, BufferedReader> p = null;
				try {
					p = htmlPages.poll();
					if (p != null) {
						consume( p );
					}
				} catch (IOException e) {
					System.out.println("failed to consume: " + p.first);
					System.out.println(e.getMessage());
				}
			}
		}
	}
	
	protected abstract void consume(Pair<String, BufferedReader> p) throws IOException;
	
	protected void skipLines(BufferedReader page, int numLines) throws IOException {
		int i = 0;
		while (i < numLines) {
			page.readLine();
			++i;
		}
	}
}