package misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class InvalidThreadParser extends Parser {

	public InvalidThreadParser(LinkedBlockingQueue<Pair<String, BufferedReader>> q) {
		super(q);
	}

	@Override
	protected void consume(Pair<String, BufferedReader> p) throws IOException {
		BufferedReader page = p.second;
		int lineNum = 400;
		String data = "";
		int pos = p.first.indexOf("?t=") + 3;
		String threadNum = p.first.substring(pos);
		
		// Approximately 400 lines of non-useful data at the start of each hmtl source
		skipLines(page, 400);
		
		while ( ( data = page.readLine() ) != null && lineNum < 500 ) {
			if (data.contains("blockrow restore") && 
				data.contains("Invalid Forum specified")) {
				// thread is invalid!
				ArrayList<String> thread = new ArrayList<>();
				thread.add(threadNum);
				outputMap.put(threadNum, thread);
				
				// exit
				lineNum = 1000;
			} // end if
		} // end while
		page.close();
	}

}
