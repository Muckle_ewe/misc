package misc;

import java.io.IOException;

public class Main {
	// This could probably just be 1. If anything the IO should be done on a few threads, though no idea what would happen there...
	private static final int NUM_THREADS = 3;
	
	public static void main(String[] args) {
		HtmlProcessor processor = new HtmlProcessor(NUM_THREADS);
		
		FileData fd = new ThreadPageFileData();
		// This really shouldn't throw an exception, all opening should surely be done before passing...
		try {
			processor.setFileData(fd);
		} catch (IOException e1) {
			System.out.println("wat");
			return;
		}
		
		Thread t = new Thread(processor);
		t.start();
		
		try {
			t.join();
		} catch (InterruptedException e) {
			System.out.println("Couldn't join threads");
		}
		
		System.out.println("All done");
	}

}
