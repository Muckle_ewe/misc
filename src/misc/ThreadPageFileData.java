package misc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Scans the full html of required pages to get all miscer user data
 * Loads ValidThreads file to get the list of threads to scan
 * Loads GlobalUserData to put known miscers into the outputMap
 * Writes to GlobalUserData
 */
public class ThreadPageFileData extends FileData {
	@Override
	public void read() throws IOException {
		outputFile = new File(desktopPath + "GlobalUserData.txt");
		if (!outputFile.exists()) {
			outputFile.createNewFile();
		} else {
			loadPreviousData();
		}

		File file = new File(desktopPath + "ValidThreads.txt");
		if (!file.exists()) {
			System.out.println("Valid Threads doesn't exist");
			return;
		}
		// Fill url arrays
		FileReader fw = new FileReader(file.getAbsoluteFile());
		BufferedReader bw = new BufferedReader(fw);
		String line = "";
		while ( (line = bw.readLine() ) != null ) {
			String[] strings = line.split("-");
			String address = new String("showthread.php?t=" + strings[0] + "&page=" + strings[1]);
			addresses.add(address);
		}
		bw.close();
	}
	
	@Override
	protected void loadPreviousData() throws IOException {
		// Kept this as separate from outputFile since wasn't sure is closing the Readers would mess things up
		File file = new File(desktopPath + "GlobalUserData.txt");
		if (!file.exists()) {
			file.createNewFile();
			return;
		}
		
		FileReader fw = new FileReader(file.getAbsoluteFile());
		BufferedReader bw = new BufferedReader(fw);
		String line = "";
		while ( (line = bw.readLine() ) != null ) {
			String[] strings = line.split(" ");
			StringBuilder name = new StringBuilder();
			
			int pos = strings.length - 1;
			String joinDate = strings[pos--];
			String age = strings[pos--];
			String weight = strings[pos--];
			String height = strings[pos--];
			String posts = strings[pos--];
			String repPower = strings[pos];
			
			// This shouldn't need to run since GlobalUserData user names should all contain the #@#
			// But just for old files which might still have spaces in names...
			for (int i = 0; i < pos; ++i) {
				name.append(strings[i]);
				if (i+1 < pos - 1) {
					name.append("#@#");
				}
			}
			ArrayList<String> val = new ArrayList<String>();
			val.add(repPower);
			val.add(posts);
			val.add(height);
			val.add(weight);
			val.add(age);
			val.add(joinDate);
			outputMap.put(name.toString(), val);
		}
		bw.close();
		fw.close();
	}

}
