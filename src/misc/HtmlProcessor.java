package misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class HtmlProcessor implements Runnable {
	private int numThreads;
	
	private int saveCount = 200;
	private LinkedBlockingQueue< Pair<String, BufferedReader> > pageQueue;
	private FileData fileData = null;
	private Parser[] parsers;
	private Thread[] threads;
	
	public HtmlProcessor() {
		this(5);
	}
	
	public HtmlProcessor(int numThreads) {
		this.numThreads = numThreads;
		
		pageQueue = new LinkedBlockingQueue<>();
		parsers = new Parser[numThreads];
		threads = new Thread[numThreads];
	}
	
	public void setFileData(FileData f) throws IOException {
		fileData = f;
		fileData.read();
	}
		
	@Override
	public void run() {
		if (fileData == null) {
		    return;
		} else {
		    createParsers();
		}
		
		ArrayList<String> addresses = fileData.getAddresses();
		int counter = 0;
		for (String address : addresses) {
			if (counter % saveCount == 0) {
				try {
					fileData.write();
				} catch (IOException e) {
					// wat do
					System.out.println("Failed to write to file at: " + saveCount);
				}
			}
			++counter;
			System.out.println(String.valueOf(counter) + " " + address);
			BufferedReader out = null;
			try {
				out = getPage("http://forum.bodybuilding.com/" + address);
				if (!out.equals(null)) {
					Pair<String, BufferedReader> p = new Pair<>(address, out);
					pageQueue.put( p );
				} else {
					System.out.println("getPage returned null for url: " + "http://forum.bodybuilding.com/" + address);
				}
			} catch (InterruptedException e) {
				System.out.println("Some thread threw an InterruptedException... Yes mad");
				// Wait for a key press maybe...
				// Fawk
			}
			// close out..?
		}
		
		finishParsing();
		
		try {
			fileData.write();
		} catch (IOException e) {
			System.out.println("IOException fail...");
			System.out.println(e.getMessage());
		}
	}
	
	private BufferedReader getPage(String url) {
		BufferedReader out;
		try {
			out = new BufferedReader(
			    new InputStreamReader( 
			        new URL(url).openStream() 
			    )
			);
		} catch (IOException | NullPointerException e) {
			out = null;
		}
			
		return out;
	}
	
	private void finishParsing() {
		for (Parser fp : parsers) {
			fp.stopReading();
		}
		try {
			for (int i = 0; i < numThreads; ++i) {
				threads[i].join();
			}
		} catch (InterruptedException e) {
			System.out.println("Couldn't join threads");
		}
	}

	private void createParsers() {
		// Shitty factory
		for (int i = 0; i < numThreads; ++i) {
			if (fileData instanceof ForumPageFileData) {
				parsers[i] = new ForumPageParser(pageQueue);
			} else if (fileData instanceof InvalidThreadFileData) {
				parsers[i] = new InvalidThreadParser(pageQueue);
			} else if (fileData instanceof PrintThreadFileData) {
				parsers[i] = new PrintThreadParser(pageQueue);
			} else {
				parsers[i] = new ThreadPageParser(pageQueue);
			}
			parsers[i].passOutputMap(fileData.getOutputMap());
			threads[i] = new Thread(parsers[i]);
			threads[i].start();
		}
	}
}
