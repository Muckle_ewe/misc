package misc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Scans a page of threads to return the numeric values of the thread titles
 * Loads previously scanned threads so that we can get any new pages from previously scanned thread
 * The list of threads and pages to scan are saved in a file named PrintThreadsToScan
 */
public class ForumPageFileData extends FileData {
	private final Integer firstPage = 1;
	private final Integer lastPage = 1201;

	@Override
	public void read() throws IOException {
		outputFile = new File(desktopPath + "PrintThreadsToScan.txt");
		if (!outputFile.exists()) {
			outputFile.createNewFile();
		} else {
			System.out.println("PrintThreadsToScan already exists");
			throw new IOException();
		}
		// Not really a read...
		for (Integer page = lastPage; page >= firstPage; --page) {
			String address = new String("forumdisplay.php?f=19&page=" + page.toString() + "&order=desc");
			addresses.add(address);
		}
		
		// Open the master file thread data, place in map so that we can figure out what new pages are needed to look at
		loadPreviousData();
	}
	
	@Override
	public void write() throws IOException {
		FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		for (String s : outputMap.keySet()) {
			ArrayList<String> val = outputMap.get(s);
			if (val.get(1) != "-1") {
				StringBuilder out = new StringBuilder();
				for (String threadPage : val) {
					out.append(threadPage + " ");
				}
				bw.write(s + " " + out.toString());
				bw.newLine();
			}
		}
		bw.close();
		fw.close();
	}
	
	@Override
	protected void loadPreviousData() throws IOException{
		// Thread data master contains list of threads with how many pages there are
		// These will have all been scanned for miscers, hence we do not need to rescan there
		File threadMasterFile = new File(desktopPath + "Master Files\\ThreadDataMaster.txt");
		if (!threadMasterFile.exists()) {
			System.out.println("Master file does not exist!");
			throw new IOException();
		}
		
		FileReader fw = new FileReader(threadMasterFile.getAbsoluteFile());
		BufferedReader bw = new BufferedReader(fw);
		String line = "";
		while ( (line = bw.readLine() ) != null ) {
			// Line is of the form...
			// threadNumber numberPages
			String[] strings = line.split(" ");
			ArrayList<String> pages = new ArrayList<>();
			// pages[0] represents the last page that has been read
			// pages[1] represents the new final page that needs to be read
			// Set this to -1 and if we find more pages for that thread, change this
			// Else -1 just means 'ignore this thread'
			pages.add(strings[1]);
			pages.add("-1");
			outputMap.put(strings[0], pages);
		}
		bw.close();
		fw.close();
	}
}
