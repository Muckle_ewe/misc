package misc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

public class InvalidThreadFileData extends FileData {
	@Override
	public void read() throws IOException {
		outputFile = new File(desktopPath + "InvalidThreads.txt");
		if (!outputFile.exists()) {
			outputFile.createNewFile();
		} else {
			throw new IOException();
		}
		
		File file = new File(desktopPath + "ToCheck.txt");
		if (!file.exists()) {
			return;
		}
 
		FileReader fr = new FileReader(file.getAbsoluteFile());
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		HashSet<String> threads = new HashSet<String>();
		while ( (line = br.readLine() ) != null ) {
			String[] strings = line.split("-");
			threads.add("showthread.php?t=" + strings[0]);
		}
		addresses.addAll(threads);
		System.out.println("Totel to scan: " + String.valueOf(threads.size()));
		br.close();
		fr.close();
	}

	@Override
	protected void loadPreviousData() throws IOException {
		//
	}
}
