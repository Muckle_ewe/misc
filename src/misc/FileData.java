package misc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Reads and writes data
 * Creates ArrayList of Strings which are the URLs to be parsed
 */
public abstract class FileData {
	protected final String desktopPath = System.getProperty("user.home") + "/Desktop\\";;
	// For reading, to be passed to the Producer
	protected ArrayList<String> addresses = new ArrayList<>();
	// For writing, to be passed to the Consumer
	protected ConcurrentHashMap<String, ArrayList<String>> outputMap;
	protected File outputFile;
	
	public FileData() {
		outputMap = new ConcurrentHashMap<>();
	}
	
	public ConcurrentHashMap<String, ArrayList<String>> getOutputMap() {
		return outputMap;
	}
	
	abstract public void read() throws IOException;
	
	abstract protected void loadPreviousData() throws IOException;
	
	public void write() throws IOException {
		FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		for (String s : outputMap.keySet()) {
			ArrayList<String> val = outputMap.get(s);
			StringBuilder out = new StringBuilder();
			for (String threadPage : val) {
				out.append(threadPage + " ");
			}
			bw.write(s + " " + out.toString());
			bw.newLine();
		}
		bw.close();
		fw.close();
	}
	
	public ArrayList<String> getAddresses() {
		return addresses;
	}

}
