/**
 * Responsible for parsing the html received from a page of the misc forum which will contain thread titles
 * Saves thread titles (in numeric form) as well as the number of pages of each thread.
 */
package misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class ForumPageParser extends Parser {
	public ForumPageParser(LinkedBlockingQueue<Pair<String, BufferedReader>> q) {
		super(q);
	}
	
	@Override
	protected void consume(Pair<String, BufferedReader> p) throws NumberFormatException, IOException {
		BufferedReader page = p.second;
		int numThreads = 0;
		String data = "";
		
		// Approximately 600 lines of non-useful data at the start of each hmtl source
		skipLines(page, 1600);
		
		while ( ( data = page.readLine() ) != null && numThreads < 25 ) {
			// To get the thread title, search for "thread_title_" which -only- appears before the threads numerical value
			// It appears after about 50 characters in a line and the thread number follows immediately from the next char.
			int titleStart = data.indexOf("thread_title_", 50);
			
			// If titleStart is -1, we did not find it in a line
			if (titleStart != -1) {
				// ">" is char after the thread number
				int titleEnd = data.indexOf(">", titleStart+13);
				
				
				if (titleEnd < 0 || titleStart + 13 < 0) {
					System.out.println("problem here with negative values: " + data);
				}
				
				// Take substring from start of the thread number to char before ">"
				String threadNum = data.substring(titleStart+13, titleEnd - 1);
				++numThreads;				

				// Map contains thread with pages that have been scanned before
				// If the thread is there, it is in the form [oldPageNums][-1]
				// If numPages is greater than oldPageNums, we have more pages to scan
				// So now will be of the form [oldPageNums][numPages]
				// If the thread isn't in the map, we have a new thread to scan
				// So will be of the form [1][numPages]
				ArrayList<String> numPageArray = new ArrayList<>();
				Integer numPages = getNumPages(page);
				if (outputMap.containsKey(threadNum)) {
					Integer oldPageNums = Integer.parseInt(outputMap.get(threadNum).get(0));
					if (numPages > oldPageNums) {
						numPageArray.add(oldPageNums.toString());
						numPageArray.add(numPages.toString());
						
						outputMap.put(threadNum, numPageArray);
					}
				} else {
					numPageArray.add("1");
					numPageArray.add(numPages.toString());
					
					outputMap.put(threadNum, numPageArray);
				}
			} // end if
		} // end while
		page.close();
	} // end function
	
	private Integer getNumPages(BufferedReader page) {
		// Each thread has a corresponding number of REPLIES for the thread
		// This is after a line which contains "othercol td-reply"
		// Format of this is...
		// <td class="othercol td-reply">
		//		NUM_REPLIES
		// </td>
		// Find the number of replies first before a new thread title is looked for
		boolean moveToNextThread = false;
		String data = "";
		Integer numPages = -1;
		while (!moveToNextThread) {
			try {
				// After about 100 lines we find the number of replies, OP post != a reply
				data = page.readLine();
				if ( !data.equals(null) && data.contains("othercol td-reply")) {

					// Move to the line with the number of replies
					data = page.readLine();
					
					// Number will contain commas so also remove them
					data = data.replaceAll("\\s", "");
					data = data.replaceAll(",", "");

					if (data.equals("-")) {
						data = "0";
					}
					
					// Number of posts is number of replies + 1
					// Number of pages is number of posts divided by 30 which gives the floor of the true result
					// Add one to find true number of pages
					try {
						numPages = (Integer.valueOf(data) + 1)/30 + 1;
					} catch (Exception e) {
						numPages = 0;
					}
					moveToNextThread = true;
				}
			} catch (Exception e) {
				System.out.println("Shit: " + data);
				// Added a thread number but don't have the thread pages, make sure both arrays are the same size
				moveToNextThread = true;
			}
		}
		
		return numPages;
	}
}
