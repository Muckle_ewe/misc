package misc;

/*
 * Responsible for retrieving usernames from the print version of a thread page
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class PrintThreadParser extends Parser {
	public PrintThreadParser(LinkedBlockingQueue< Pair<String, BufferedReader> > q) {
		super(q);
	}
		
	@Override
	protected void consume(Pair<String, BufferedReader> p) throws IOException {
		BufferedReader page = p.second;
		String threadPage = getThreadPage(p.first);
		
		String data = "";
		String lastUserName = "";
		int numberOfPosters = 0;
		while ( (data = page.readLine()) != null && numberOfPosters < 30) {
			// What we look for is something of the form...
			// <span class="username">...</span>
			if (data.contains("span class")) {
				int startIndex = data.indexOf("username", 10);
				if (startIndex != -1) {
					int endIndex = data.indexOf("<", startIndex + 8);
					// username is contained between the ">" and "<"
					lastUserName = data.substring(startIndex + 10, endIndex);
					lastUserName = lastUserName.replace(" ", "#@#");
					
					// Update the threads associated with that username
					ArrayList<String> value;
					if (outputMap.containsKey(lastUserName)) {
						value = outputMap.get(lastUserName);
					} else {
						// New name, create new HashSet
						value = new ArrayList<String>();
					}
					if (!value.contains(threadPage)) {
						value.add(threadPage);
					}
					outputMap.put(lastUserName, value);
				} // end if
			} // end if data.contains("span class");
		} // end while
		page.close();
	}
	
	private String getThreadPage(String address) {
		int startOfThreadNum = address.indexOf("?t=") + 3;
		int ppStart = address.indexOf("&pp");
		
		String threadNumber = address.substring(startOfThreadNum, ppStart);
		String pageNumber = address.substring(ppStart + 12, address.length());
		String threadPage = threadNumber + "-" + pageNumber;
		
		return threadPage;
	}
}