package misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadPageParser extends Parser {
	private String repPower = "-1";
	private String posts = "-1";
	private String height = "none"; // inches
	private String weight = "-1"; // lbs
	private String age = "-1";
	private String joinDate = "none";
	
	public ThreadPageParser(LinkedBlockingQueue<Pair<String, BufferedReader>> q) {
		super(q);
	}
	
	@Override
	protected void consume(Pair<String, BufferedReader> p) throws IOException {		
		BufferedReader page = p.second;
		skipLines(page, 600);
		
		String data = "";
		String lastUserName = "";
		int numberOfPosters = 0;
		boolean mapUserName = false;
		// Patterns we are looking for.
		// " Avatar": username will appear 16 chars after this
		// "[username]'s Avatar": [username] is the username and we use this to find the end of it, appears on same line as " Avatar"
		// "<dt>": blocks contain the meta data
		// " Avatar" appears before Rep power so we only search for "<dt>" if we've found a new userName
		// Hence mark mapUserName as true if new name found, false otherwise
		// Map username to the meta data
		// Skip 200 lines if you find that the map already has a username.
		while ( (data = page.readLine()) != null && numberOfPosters < 30 ) {
			int startAvatar = data.indexOf(" Avatar");
			if ( startAvatar != -1 ) {
				int startSecondAvatar = data.indexOf("s Avatar", startAvatar + 16);
				
				lastUserName = data.substring(startAvatar + 16, startSecondAvatar - 1);
				
				// Spaces are annoying for file reading... Make a recognisable token to replace them with
				lastUserName = lastUserName.replace(" ", "#@#");
				
				if (outputMap.containsKey(lastUserName)) {
					mapUserName = false;
					numberOfPosters++;
					skipLines(page, 200);
				} else {
					mapUserName = true;
				}
			} else if (mapUserName && data.length() > 10 && data.contains("<dt>")) {
				// Whole lotta repetition here. Could probably make 1-2 functions to do everything
				if (data.contains("Location:")) {
					// skip to prevent any locations which contain Posts:, Rep Power: etc...
				} else if (data.contains("Join Date:")) {
					addJoinDataToUserData(data);
				} else if (data.contains("Age:")) {
					addAgeToUserData(data);
				} else if (data.contains("Stats:")) {
					addStatsToUserData(data);
				} else if (data.contains("Posts")) {
					addPostsToUserData(data);
				} else if (data.contains("Rep Power:")) {
					addRepPowerToUserData(data);
					numberOfPosters++;
					skipLines(page, 150);
					mapUserName = false;
					// Overwrite the old one as this may contain newer data
					ArrayList<String> userData = new ArrayList<>();
					userData.add(repPower);
					userData.add(posts);
					userData.add(height);
					userData.add(weight);
					userData.add(age);
					userData.add(joinDate);
					
					outputMap.put(lastUserName, userData);
					repPower = "-1";
					posts = "-1";
					height = "none"; // inches
					weight = "-1"; // lbs
					age = "-1";
					joinDate = "none";
				}			
			}
		} // end while
		page.close();
	}

	private void addAgeToUserData(String data) {
		int start = data.indexOf(":");
		int end = data.indexOf("<", start);
		age = data.substring(start + 2, end);
	}

	private void addRepPowerToUserData(String data) {
		int start = data.indexOf(">", 30);
		int end = data.indexOf("<", start);
		repPower = data.substring(start + 1, end);
	}

	private void addPostsToUserData(String data) {
		int start = data.indexOf(":");
		int end = data.indexOf("<", start);
		String posts1 = data.substring(start + 2, end);
		posts = posts1.replaceAll(",", "");
	}

	private void addStatsToUserData(String data) {
		int start = data.indexOf(":");
		int end = data.indexOf(",", start);
		if (end == -1) {
			end = start;
		} else {
			height = data.substring(start + 2, end - 1);
		}
		if (data.contains("lbs")) {
			int end2 = data.indexOf("<", end);
			weight = data.substring(end + 2, end2 - 4);
		}
	}

	private void addJoinDataToUserData(String data) {
		int start = data.indexOf(":");
		int end = data.indexOf("<", start);
		String jd = data.substring(start + 2, end);
		joinDate = jd.replace(" ", "_");
	}
}
