#ifndef THREADUSERS_H
#define THREADUSERS_H

#include <string>
#include <unordered_set>

// Class that holds a thread (thread number and page) and their unordered_set of users who have posted on the page, just really to make the main algo easier to write.

class ThreadUsers {
    public:
        ThreadUsers();
        ThreadUsers(const std::string& t, const std::unordered_set<std::string> &u);

        std::string getThreadNumber() const;
        int getNumberOfUsers() const;

        std::unordered_set<std::string> getUsers() const {return users;}

        bool removeUsers(const ThreadUsers& t) {
            for (auto it = t.users.begin(); it != t.users.end(); ++it) {
                auto f = users.find(*it);
                if (f != users.end()) {
                    users.erase(f);
                    if (users.empty()) {
                        return true;
                    }
                }
            }
            return users.empty();
        }

        bool compareNumberOfUsers(const ThreadUsers& rhs) const;

        friend bool operator < (const ThreadUsers& t1, const ThreadUsers& t2);
        friend bool operator == (const ThreadUsers& t1, const ThreadUsers& t2);

    private:
        std::string threadNumber;
        std::unordered_set<std::string> users;
};

#endif // THREADUSERS_H
