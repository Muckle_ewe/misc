#include "UserThreads.h"

UserThreads::UserThreads() {
}

UserThreads::UserThreads(const std::string &u, const std::unordered_set<std::string> &t) :
    userName(u),
    threads(t) {
}

void UserThreads::setUserName(const std::string &u) {
    userName = u;
}

void UserThreads::setThreads(const std::unordered_set<std::string> &t) {
    threads = t;
}

int UserThreads::getNumberOfThreads() const {
    return threads.size();
}

bool UserThreads::containsThreadFrom(const std::unordered_set<std::string>& threadSet) const {
    /*for (auto it = threadSet.begin(), ite = threadSet.end(); it != ite; ++it) {
        if (threads.find(*it) != threads.end()) {
            return true;
        }
    }
    return false;*/
    for (auto s : threadSet) {
        if (threads.find(s) != threads.end()) {
            return true;
        }
    }
    return false;
}

bool UserThreads::compareNumberOfThreads(const UserThreads& rhs) const {
    return ( threads.size() == rhs.threads.size() ) ? userName < rhs.userName : threads.size() < rhs.threads.size();
}
