#ifndef USERTHREADS_H
#define USERTHREADS_H

#include <set>
#include <string>
#include <unordered_set>

// Class that holds a username and their unordered_set of threads, just really to make the main algo easier to write.

class UserThreads {
    public:
        UserThreads();
        UserThreads(const std::string &u, const std::unordered_set<std::string> &t);

        void setUserName(const std::string &u);
        void setThreads(const std::unordered_set<std::string> &t);

        int getNumberOfThreads() const;
        std::string getUserName() const {return userName;}
        std::unordered_set<std::string> getThreads() const {return threads;}

        friend bool operator < (const UserThreads& lhs, const UserThreads& rhs);
        friend bool operator == (const UserThreads& lhs, const UserThreads& rhs);

        bool compareNumberOfThreads(const UserThreads& rhs) const;
        bool containsThreadFrom(const std::unordered_set<std::string>& threadSet) const;

    private:
        std::string userName;
        std::unordered_set<std::string> threads;
};

#endif // USERTHREADS_H
