#ifndef MINIMIZETHREADS_H
#define MINIMIZETHREADS_H

// Creates a .txt file with the threads to do a full scan on
// Calculates the minimum (not true min but likely close to it) number of threads needed in order to scan every known miscer
void createMinimalThreadList();

#endif // MINIMIZETHREADS_H
