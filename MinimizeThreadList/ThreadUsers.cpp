#include "ThreadUsers.h"

ThreadUsers::ThreadUsers() {
}

ThreadUsers::ThreadUsers(const std::string& t, const std::unordered_set<std::string> &u) :
    threadNumber(t),
    users(u) {
}

std::string ThreadUsers::getThreadNumber() const {
    return threadNumber;
}

int ThreadUsers::getNumberOfUsers() const {
    return users.size();
}

bool ThreadUsers::compareNumberOfUsers(const ThreadUsers& rhs) const {
    return ( users.size() == rhs.users.size() ) ? threadNumber < rhs.threadNumber : users.size() > rhs.users.size();
}
