#include "MinimizeThreads.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <sstream>
#include <unordered_map>
#include <vector>
#include "ThreadUsers.h"
#include "UserThreads.h"

using namespace std;

const string desktopPath = "C:\\Users\\B\\Desktop\\";

typedef unordered_map<string, set<string>> StringSetMap;

StringSetMap makeUserToThreadMap();
StringSetMap makeThreadToUserMap(const StringSetMap &userToThreadMap);
list<string> handleSingleThreadPosters(list<UserThreads> &userThreads);
list<string> handleThreadUsersList(list<ThreadUsers> &threadUsers);
void parseLine(const string &line, string &name, set<string> &threads);

template<typename T>
list<T> mapToList(const StringSetMap& inputMap);

// I'm sure pre-built templates, less<>, etc, can do this but...
bool operator < (const UserThreads& lhs, const UserThreads& rhs) {
    return lhs.compareNumberOfThreads(rhs);
}

bool operator == (const UserThreads& lhs, const UserThreads& rhs) {
    return lhs.threads.size() == rhs.threads.size();
}

bool operator < (const ThreadUsers& t1, const ThreadUsers& t2) {
    return t1.compareNumberOfUsers(t2);
}

bool operator == (const ThreadUsers& t1, const ThreadUsers& t2) {
    return t1.getNumberOfUsers() == t2.getNumberOfUsers();
}

/*
 * createMinimalThreadList: Creates a minimal list of thread pages to be scanned in order to find ever miscer
 */
void createMinimalThreadList() {
    StringSetMap userToThreadMap = makeUserToThreadMap();
    if (userToThreadMap.empty()) {
        cout << "userToThreadMap was empty.";
        return;
    }

    list<UserThreads> userThreads = mapToList<UserThreads>(userToThreadMap);
    userThreads.sort();

    // So now userThreads are in order of users against number of threads they've posted in, with lowest first.
    // Now we minimize the number of threads, first by dealing with miscers who have posted in only one thread.
    cout << "old: " << userThreads.size() << endl;
    list<string> validThreads = handleSingleThreadPosters(userThreads);
    cout << "new: " << userThreads.size() << endl;

    userToThreadMap.clear();
    for (auto u : userThreads) {
        auto threads = u.getThreads();

        set<string> setOfThreads;
        copy(threads.begin(), threads.end(), inserter(setOfThreads, setOfThreads.begin()));

        userToThreadMap.insert( { u.getUserName(), setOfThreads } );
    }

    StringSetMap threadToUserMap = makeThreadToUserMap(userToThreadMap);

    list<ThreadUsers> threadUsers = mapToList<ThreadUsers>(threadToUserMap);
    threadUsers.sort();

    list<string> temp = handleThreadUsersList(threadUsers);
    copy(temp.begin(), temp.end(), back_inserter(validThreads));
    validThreads.sort();

    // To be renamed to ValidThreads once it's checked/changed/filtered/whatever
    ofstream os(desktopPath + "TestingWrite.txt");
    copy(validThreads.begin(), validThreads.end(), ostream_iterator<string>(os, "\n"));

    os.close();
}

/*
 * makeUserToThreadMap: Retrieves master user to thread file and places it into an unordered_map
 * @return: An unordered_map<string, set<string>> where key is username and
 *          value is the set of thread pages a miscer has posted in.
 * @param: globalUserDate. A string representing the date from which we take the GlobalUserData from to make an ignore list
 */
StringSetMap makeUserToThreadMap() {
    StringSetMap userToThreadMap;

    ifstream is(desktopPath + "Master Files\\UserToThreadMaster.txt");
    ifstream miscersToIgnore(desktopPath + "Master Files\\MiscersToIgnoreMaster.txt");

    /** Remove the miscersToIgnore redo every miscer **/
    if ( !(is && miscersToIgnore) ) {
        return StringSetMap();
    }

    // A miscer is ignored if they have no posts or change in rep power since the original list
    unordered_set<string> ignoreList;
    copy(istream_iterator<string>(miscersToIgnore),
         istream_iterator<string>(),
         inserter(ignoreList, ignoreList.begin()));

    string line;
    while (getline(is, line)) {
        string name = "";
        set<string> threads;
        parseLine(line, name, threads);

        if ( name != "" && !threads.empty() && ignoreList.count(name) == 0) {
            userToThreadMap.insert( { name, threads } );
        }
    }
    is.close();
    miscersToIgnore.close();

    return userToThreadMap;
}

/*
 * makeThreadToUserMap: Reversing the user to thread map so that we have a map of thread pages to users posting in them.
 * @return: unordered_map<string, set<string>>. A map with the key as thread page and value as set of users who post in them
 *
 * @param: userToThreadMap. The user to thread page map where key is user and value is the set of threads pages the user posted in
 */
StringSetMap makeThreadToUserMap(const StringSetMap &userToThreadMap) {
    StringSetMap threadToUserMap;

    for (auto it = userToThreadMap.begin(); it != userToThreadMap.end(); ++it) {
        auto userName = it->first;
        auto threads = it->second;
        for (string thread : threads) {
            if (threadToUserMap.find(thread) != threadToUserMap.end()) {
                threadToUserMap[thread].insert(userName);
            } else {
                set<string> s;
                s.insert(userName);
                threadToUserMap.insert( { thread, s } );
            }
        }
    }

    return threadToUserMap;
}

/*
 * handleSingleThreadPosters: Takes miscers that have posted in only a single thread, saves the thread,
 *     then removes this thread from any other miscers thread set. If this reduces a miscers thread set
 *     to no threads they are removed from the userThreads list.
 * @return: list<string>. A list of the threads found by the single threads posters
 * @param: userThreads. The list containing the user and thread pages they have posted in,
 *      sorted in ascending number of thread pages they've post in.
 */
list<string> handleSingleThreadPosters(list<UserThreads> &userThreads) {
    list<string> validThreads;
    unordered_set<string> threads;

    auto it = userThreads.begin();
    while ( it->getNumberOfThreads() == 1 ) {
        threads.insert( *(it->getThreads().begin()) );
        it = userThreads.erase(it);
    }

    userThreads.erase( remove_if(userThreads.begin(), userThreads.end(),
                       [&](UserThreads ut){return ut.containsThreadFrom(threads);}),
                       userThreads.end() );

    copy(threads.begin(), threads.end(), back_inserter(validThreads));
    return validThreads;
}

/*
 * handleThreadUsersList: Uses the list of thread pages to users to minimize the thread pages to be checked
 * @return: list<string>. A list containing the thread pages to be checked.
 * @param: threadUsers. A list of thread pages with the users that have posted in them, sorted in ascending
 *       order with regards to the number of users.
 */
list<string> handleThreadUsersList(list<ThreadUsers> &threadUsers) {
    list<string> validThreads;
    auto it = threadUsers.begin();
    cout << "Size to look through... " << threadUsers.size() << endl;
    int counter = 0;
    int sizeThreadUsers = threadUsers.size();

    // The front of threadsUsers will contain the thread with most users. Take users from this threads
    // and remove them from any other thread page they've posted in. If the thread page then has
    // no users left, it is removed.
    while (!threadUsers.empty()) {
        if (counter % 100 == 0) {
            cout << counter << " " << it->getThreadNumber() << " " << sizeThreadUsers << endl;
        }
        ++counter;

        auto it2 = it;
        ++it2;
        for (; it2 != threadUsers.end(); ) {
            if (it2->removeUsers(*it)) {
                it2 = threadUsers.erase(it2);
                --sizeThreadUsers;
            } else {
                ++it2;
            }
        }

        validThreads.push_back(it->getThreadNumber());
        threadUsers.erase(it);
        --sizeThreadUsers;

        // Re-sort the list after each removal of the front thread
        threadUsers.sort();
        it = threadUsers.begin();
    }
    return validThreads;
}

/*
 * parseLine: Retreives the username and thread set from the input string
 * @param: line. The string from the userToThreadData file representing
 *         the miscer and what thread pages they've post in
 * @param: name. An output string that is assigned the username
 * @param: threads. An output set<string> to be filled with thread pages the miscer has posted in
 */
void parseLine(const string &line, string &name, set<string> &threads) {
    vector<string> data;
    stringstream ss(line);

    copy(istream_iterator<string>(ss), istream_iterator<string>(),
         back_inserter(data));

    name = data[0];
    copy(data.begin() + 1, data.end(), inserter(threads, threads.begin()));
}

/*
 * mapToList: Converts a map of user to thread pages or thread pages to users to a list of UserThreads or ThreadUsers
 * @return: list<T>. A list of UserThreads of ThreadUsers
 * @param: inputMap. An unordered_map where key is a string (user/thread page) and value is a set of strings (thread pages/users)
 */
template<typename T>
list<T> mapToList(const StringSetMap& inputMap) {
    list<T> outputList;
    for (auto it = inputMap.begin(), ite = inputMap.end(); it != ite; ++it) {
        auto val = it->second;
        unordered_set<string> stringSet(val.begin(), val.end());

        T u(it->first, stringSet);
        outputList.push_back(u);
    }

    return outputList;
}
